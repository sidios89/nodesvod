var expect = require("chai").expect;
var request = require("request");

describe("Default spec", function() {
    it("return my favorite film", function(done) {
        var url = "http://localhost:3000/myfavoritefilm";
        request(url, function(error, response, body) {
            expect(body).to.equal("interstellar");
            done();
        })
    })
})